class: center, middle

.presentationtitle[
# Python I <br>Syntax, lists, control flow and functions
]

Renato Alves - EMBL Heidelberg<br>
Marta Strumillo - EMBL-EBI

BTM - October 2016

[https://git.embl.de/ralves/intro_python](https://git.embl.de/ralves/intro_python)

???

Talk about PUG@EMBL and Coding club

---

# Setting up

If you installed anaconda, create a new project folder, open a terminal in that folder and run `jupyter notebook`.

???

Show of hands for who did the installation

--

If you didn't or have problems running it use [http://ralves.embl.de:8888](http://ralves.embl.de:8888).
Create a folder with your name and then a new Python 3 document.

???

Shared setup - behave or we'll all suffer :)

--

During the presentation we'll have several moments to try things out.
Use Jupyter to make things easier.

???

Brief intro on how to use Jupyter. Write in the cell and press Ctrl/Shift + Enter.

---

# What is Python?

* A generalist programming language
* Supports many styles (object-oriented, imperative, functional,...)

???

Meaning it can be used for any kind of problem.
Object-oriented being the most popular style

--

* Quite popular due to several reasons including:
    * *Easy to learn*
    * *Easy to read*
    * *Easy to work with*
    * *Saves the coders' time*
    * Zen of Python - `import this`

???

Opinionated but mostly means people enjoy it and are productive with it.

--

* Rich community with modules for most tasks. [PyPi](https://pypi.python.org)
* A lot of [organizations use it](https://wiki.python.org/moin/OrganizationsUsingPython) (Google, Dropbox, NASA,...)

--

* Often frowned upon due to being *slow*.
* In the beginning, indentation and lack of braces. `import braces`

???

Performance sensitive may be attained with cython or external modules

---

# The two versions

* Python 2
    * Older, still widely in use, support ends in 2020
    * If you are starting, skip it if you can

--

* Python 3
    * Newer generation with lots of improvements
    * Quite a few differences from version 2
    * Generally considered better than 2
    * Not yet in full use, some software may only run on version 2

---

# How does it look like?

```python
# A file with one number per line

myfile = "/data/myresult.txt"

cutoff = 5.0

data = {
    "bigger": [],
    "smaller": [],
}

with open(myfile) as f:
    for line in f:
        number = float(line)
        if number > cutoff:
            data["bigger"].append(number)
        else:
            data["smaller"].append(number)

bigmean = sum(data["bigger"]) / len(data["bigger"])
smallmean = sum(data["smaller"]) / len(data["smaller"])

print("The mean of my big data is:", bigmean)
print("The mean of my small data is:", smallmean)
```


---

# Comments

```python
# A file with one number per line
```

Any line starting with a `#` sign is considered a comment

--

Python doesn't have block comment markers, you have to prefix every line with `#`.

```python
# A multi
# line
# comment
```

---

# Variables

```python
myfile = "/data/myresult.txt"
```

`myfile` is a `variable`.

In Python a variable is just a name or reference to some content.
During execution it can reference different things.

--

The following is perfectly valid.

```python
me = "Jane"
me = 42
me = ["a", "list", "of", "words"]
```

---

# Operators

```python
myfile = "/data/myresult.txt"
```

The little `=` sign after the variable is an operator.
In this case it means `assign`, much like how you do it in a mathematical context.

--

In addition python has the following operators:

`+`, `-`, `*`, `/`, `//`, `**`, `%`

You'll probably recognize the first 4.

--

The other three are *integer division*, *exponentiation* (aka power) and *modulo* (aka remainder).

???

A moment now so people can try them on their console.

--

There's also comparison operators:

`==`, `!=`, `>=`, `<=`, `>`, `<` and `is` as well as the logical `and`, `or` and `not`.

for *equal*, *not equal*, *greater or equal*, *smaller or equal*, *greater than*, *smaller than* and *identity*.

--

This list is non-exhaustive. There's more operators but you won't find them frequently.

---

# Strings

```python
myfile = "/data/myresult.txt"
```

The part after the `=` represents a `string`.

--

In python you can use single `'` or double quotes `"` indistinguishably.

```python
good = "Why don't we do this?"
bad  = 'But this won't work'
```

--

There're also multiline strings which you can do with triple quotes `"""` or `'''`.

```python
poem = """
Roses are red
Violets are blue
I like Python
and so will you
"""
```

--

Python 3 also has `bytes`, more on this in the **Python II - IO and Plotting** session.

---

# Numbers

```python
cutoff = 5.0
```

We mentioned operators, but what would be their use without numbers...

--

Python knows different kinds of numbers:

`int`, `float`, `complex` and also `oct` and `hex`

--

They represent *integers* (aka base 10), *floating point*, *complex*, *octal* (aka base 8), *hexadecimal* (aka base 16)

```python
int("2")
float("2.5")
complex("2+1j")
oct("0o6")
hex("0x2")
```

---

# Lists, tuples and sets

```python
data = {
    "bigger": [],
    "smaller": [],
}
```

Lists are those `[ ]` symbols. You can also create a list with the `list` function.
They are generic purpose containers and can hold any kind of object.

--

```python
mylist = ["I", "am", "learning", "python"]
print(mylist[0])    # will output "I"
print(mylist[0:2])  # will output ["I", "am"]
mylist[2] = "a pro at"
print(mylist)       # will output ["I", "am", "a pro at", "python"]
mylist.append("tomorrow")
print(mylist)       # will output ["I", "am", "a pro at", "python", "tomorrow"]
del mylist[2]       # removes value at position 2
```

--

`tuples` are a different and more rigid kind of lists. You can't modify them, only read from them and create new ones.
They use the `( )` syntax with at least one comma. E.g. `(1,)` or `(1, 2)`

???

Sometimes parenthesis are not required. Keep this in mind!

--

and then `sets`. These are super efficient for some operations that require comparison of contents. Think *Venn diagram*. They don't have a special character. You create them by calling the set function: `set(mylist)`.

---

# Dictionaries

```python
data = {
    "bigger": [],
    "smaller": [],
}
```

We mentioned `[ ]` and we also have `{ }`, dictionaries. Or also via the `dict` function.
They hold pairs of data in the form `key : value`.

--

```python
data = {"me": "you"}
print(data["me"])   # will output "you"
data[5] = "snakes"
print(data[5])      # will output "snakes"
del data[5]         # removes key 5 its value
```

--

Not everything can be a key, but anything can be a value.

```python
a = ["list", "words"]
data["me"] = a   # no problem here
data[a] = "me"   # won't work
```

---

# Logical blocks

Python uses indentation as part of its syntax.

```python
with open(myfile) as f:
    for line in f:
        number = float(line)
        if number > cutoff:
            data["bigger"].append(number)
        else:
            data["smaller"].append(number)
```

This means that if a block moves forward, it belongs under the previous block.
If it moves back, it's no longer part of that block. Notice the `else` moving back one level.

--

Notice also that forward indentation usually follows after a `:` (colon) in the previous line.

--

It also means that if you mess it up you will either get errors or the result will not be what you expect.

--

It feels hard but it will become natural as you go.

A general recommendation is to use spaces for indentation, most commonly *4 spaces*.

???

Editors can take care of this for you, no need to add the 4 spaces by hand

---

# with statement

```python
with open(myfile) as f:
    # beginning with
    ...

# end of with
```

It does as it reads, opens the file for reading and stores a reference to the file as `f`.

--

The `with` keyword is an advanced feature to implement, but easy to use.
It basically does something at the beginning and at the end of each block.

In this case, it opens a file at the beginning and takes care of closing it when we are done.

???

The `:` colon at the end again.

---

# if elif else

```python
if number > cutoff:
    ...
else:
    ...
```

--

The `if` statement is a frequent way of expressing conditions.

In the example it reads as in english *if the number is bigger or equal to our cuttoff* done something, otherwise do something else instead.

--

It can also have the form:

```python
if number > cutoff:
    ...
elif number == cutoff:
    ...
else:
    ...
```

???

And again indentation and ':'

---

# for and while loops

```python
with open(myfile) as f:
*   for line in f:
        ...
```

Python has two types of loops. Iterative `for` and conditional `while`.

--

The `for` loop is most commonly used to iterate data until it's exhausted. In the example we are looping over each line of the file.

--

Additionally you also have `while` which loops until a condition is met:

```python
count = 0
while count < 10:
    print(count)   # prints numbers 0 to 9
    count += 1
```

--

As data tends to be finite, `for` loops are considered safer. `while` can lead to infinite loops by accident.

???

Can you think of what would be the most common infinite loop case?
Don't run this on Jypiter or kernel restart!

---

# Functions

```python
bigmean = sum(data["bigger"]) / len(data["bigger"])
smallmean = sum(data["smaller"]) / len(data["smaller"])
```

Here we see two functions being used: `sum` and `len`.
These compute the *sum* of all elements in a list and the *length* or size of that list.

--

If you want to define your own functions:

```python
def myfunction(param1, param2=6):
    """This function does math"""
    return param1 * param2

myfunction(5)                   # output 30
myfunction(5, 2)                # output 10
myfunction(param2=3, param1=5)  # output 15
```

---

# Imports

```python
import antigravity          # Only works on your computer
```

Importing is how you bring additional functionality to the current workspace.
It's also how you reuse your functions outside of the script where they are written.

--

Creating reusable code requires some practice but imports will be with you from day one.

--

The syntax can have a few variations. The following are all valid.


```python
import matplotlib
from matplotlib import pyplot
import numpy as np
```

---

# Exceptions and tracebacks

*OMG an error! All went to hell now!...*

Fear not, exceptions are there to help you.

```python
file = "oops_file.txt"
open(file)

Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
FileNotFoundError: [Errno 2] No such file or directory: 'oops_file.txt'
```

???

Take a moment to analyse the error

--

It's clear what's wrong, but how to handle it?

--

The solution is to use `try` and `except` to *catch* the exception and handle it.

```python
file = "oops_file.txt"
try:
    open(file)
except FileNotFoundError:
    print("File", file, "doesn't exist so lets skip it")
```

--

Only uncaught/unhandled exceptions will cause your program to crash/exit.

---

# Longer tracebacks


```python
Traceback (most recent call last):
  File "bar.py", line 14, in c
    foo.d()
  File "foo.py", line 5, in d
    e()
  File "foo.py", line 8, in e
    f()
  File "foo.py", line 11, in f
    raise FooException("There's some problem ...")
foo.FooException: There's some problem ...

During handling of the above exception, another exception occurred:

Traceback (most recent call last):
  File "<string>", line 1, in <module>
  File "bar.py", line 7, in a
    b()
  File "bar.py", line 10, in b
    c()
  File "bar.py", line 16, in c
    raise BarException(e)
bar.BarException: There's some problem ...
```

---

class: center, middle

# Actual programming

--

Programming is often 60-90% time debug<br>
the remainder used during design & implementation

---

# Some exercises

```python
a = "5"
b = "6"
a * b
```

--

```python
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-1-0598934aecb9> in <module>()
      1 a = "5"
      2 b = "6"
----> 3 a * b

TypeError: can't multiply sequence by non-int of type 'str'
```

---

# Some exercises

```python
a = "5"
b = int("6")
a * b
```

--

```python
'555555'
```

---

# Some exercises

```python
a = "5"
b = float("6")
a * b
```

--

```python
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-3-0762798ba982> in <module>()
      1 a = "5"
      2 b = float("6")
----> 3 a * b

TypeError: can't multiply sequence by non-int of type 'float'
```

---

# Some exercises

```python
a = [1, 2, 3]
sum(a)
```

--

```python
6
```

---

# Some exercises

```python
a = [1, 2, 3, "4"]
sum(a)
```

--

```python
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-5-0282c33d4586> in <module>()
      1 a = [1, 2, 3, "4"]
----> 2 sum(a)

TypeError: unsupported operand type(s) for +: 'int' and 'str'
```

---

# Some exercises

```python
a = [1, 2, 3)
sum(a)
```

--

```python
  File "<ipython-input-6-551d3a3af22d>", line 1
    a = [1, 2, 3)
                ^
SyntaxError: closing parenthesis ')' does not match opening parenthesis '['
```

---

# Some exercises

```python
a = [1, 2, 3]
sum[a]
```

--

```python
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-7-712016ff3c8a> in <module>()
      1 a = [1, 2, 3]
----> 2 sum[a]

TypeError: 'builtin_function_or_method' object is not subscriptable
```

---

# Some exercises

```python
a = 0
b = 5
a / b
```

--

```python
0
```

---

# Some exercises

```python
a = 0
b = 5
b / a
```

--

```python
---------------------------------------------------------------------------
ZeroDivisionError                         Traceback (most recent call last)
<ipython-input-8-26180309fd40> in <module>()
      1 a = 0
      2 b = 5
----> 3 b / a

ZeroDivisionError: division by zero
```

---

# Some exercises

```python
a = [1, 2, 3]
a[3]
```

--

```python
---------------------------------------------------------------------------
IndexError                                Traceback (most recent call last)
<ipython-input-9-c446237d8751> in <module>()
      1 a = [1, 2, 3]
----> 2 a[3]

IndexError: list index out of range
```

---

# Some exercises

```python
a = [1, 2, 3]
a.append(a)
```

--

```python
[1, 2, 3, [...]]
```

---

# Some exercises

```python
a = [1, 2, 3]
a.append(a)
a == a[3][3]
```

--

```python
True

# a[3] is actually a
# a[3][3] in this case it's the same as a[3]
```

---

# Some exercises

```python
a = {"one": 1, "two": 2}
a[1]
```

--

```python
---------------------------------------------------------------------------
KeyError                                  Traceback (most recent call last)
<ipython-input-11-cbbb5cc40016> in <module>()
      1 a = {"one": 1, "two": 2}
----> 2 a[1]

KeyError: 1
```

---

# Some exercises

```python
a = {"one": 1}
a["two"] = a
```

--

```python
{'one': 1, 'two': {...}}
```

---

# Some exercises

```python
a = {"one": 1}
a[a] = "two"
```

--

```python
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-14-362d55ef5f0e> in <module>()
      1 a = {"one": 1}
----> 2 a[a] = "two"

TypeError: unhashable type: 'dict'
```

---

class: center, middle

# That's all folks!
